<?php
/*!
 * Traq
 * Copyright (C) 2009-2013 Traq.io
 *
 * This file is part of Traq.
 *
 * Traq is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3 only.
 *
 * Traq is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Traq. If not, see <http://www.gnu.org/licenses/>.
 */

namespace traq\plugins;

use \FishHook;
use \HTML;
use avalon\Autoloader;
use avalon\Database;
use avalon\http\Router;
use avalon\http\Request;
use avalon\output\View;





use Vcs\models\VcsModel;

/**
 * Custom tabs plugin.
 *
 * @since 3.0.7
 * @package Traq
 * @subpackage Plugins
 * @author Jack P.
 * @copyright (c) Jack P.
 */
class Vcs extends \traq\libraries\Plugin
{
    protected static $info = array(
        'name'    => 'VCS',
        'version' => '1.0',
        'author'  => 'Timo B.'
    );

    private static $tabs = array();

    public static function init()
    {
        // Register namespace
        Autoloader::registerNamespace('Vcs', __DIR__);

        // Add routes
        Router::add('/admin/vcs', 'Vcs::controllers::admin::Vcs.index');
        Router::add('/admin/vcs/new', 'Vcs::controllers::admin::Vcs.new');
        Router::add('/admin/vcs/([0-9]+)/(edit|delete)', 'Vcs::controllers::admin::Vcs.$2/$1');

        


        Router::add('/' . RTR_PROJSLUG . '/repository/([0-9]+)',      'Vcs::controllers::Vcs.index/$2');
        Router::add('/' . RTR_PROJSLUG . '/repository/([0-9]+)/tree', 'Vcs::controllers::Vcs.tree/$2');
        Router::add('/' . RTR_PROJSLUG . '/repository/([0-9]+)/commit/([a-zA-Z0-9]+)', 'Vcs::controllers::Vcs.commit/$2,$3');
        Router::add('/' . RTR_PROJSLUG . '/repository/([0-9]+)/tree/raw', 'Vcs::controllers::Vcs.raw/$2');
        Router::add('/' . RTR_PROJSLUG . '/repository/([0-9]+)/history', 'Vcs::controllers::Vcs.history/$2');

        // Hook into the admin navbar
        FishHook::add('template:layouts/admin/main_nav', array(get_called_class(), 'admin_nav'));

        // Get tabs
        static::$tabs = VcsModel::fetch_all();

        View::set('vcs', static::$tabs);

        // Hook into navbar
        FishHook::add('template:layouts/default/main_nav', array(get_called_class(), 'display_tabs'));
    }

    /**
     * Display tabs
     */
    public static function display_tabs()
    {
        echo View::render('vcs/tabs');
    }

    /**
     * Add link to AdminCP nav.
     */
    public static function admin_nav()
    {
        echo '<li' . iif(active_nav('/admin/vcs'), ' class="active"') . '>' . HTML::link(l('vcs'), "/admin/vcs") . '</li>';
    }

    /**
     * Create the tabs table
     */
    public static function __install()
    {
        Database::connection()->query("
            DROP TABLE IF EXISTS `vcs`;
            CREATE TABLE `" . Database::connection()->prefix . "vcs` (
              `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
              `type` varchar(255) NOT NULL DEFAULT '',
              `name` varchar(255) NOT NULL DEFAULT '',
              `path` varchar(255) NOT NULL DEFAULT '',
              `project_id` int(11) NOT NULL,
              `groups` varchar(255) NOT NULL DEFAULT '',
              `cloneurl` varchar(512) NOT NULL DEFAULT '',
              `display_order` int(11) NOT NULL,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

            DROP TABLE IF EXISTS `vcs_log`;
            CREATE TABLE `" . Database::connection()->prefix . "vcs_log` (
              `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
              `project_id` int(11) NOT NULL,
              `commit` varchar(512) NOT NULL DEFAULT '',
              `author` varchar(512) NOT NULL DEFAULT '',
              `date` text,
              `message` text,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

            DROP TABLE IF EXISTS `vcs_commit`;
            CREATE TABLE `" . Database::connection()->prefix . "vcs_commit` (
              `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
              `project_id` int(11) NOT NULL,
              `commit` varchar(512) NOT NULL DEFAULT '',
              `author` varchar(512) NOT NULL DEFAULT '',
              `date` text,
              `info` text,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

        ");
    }

    /**
     * Delete the tabs table
     */
    public static function __uninstall()
    {
        Database::connection()->query("DROP TABLE IF EXISTS `" . Database::connection()->prefix . "vcs`;");
        Database::connection()->query("DROP TABLE IF EXISTS `" . Database::connection()->prefix . "vcs_log`;");
        Database::connection()->query("DROP TABLE IF EXISTS `" . Database::connection()->prefix . "vcs_commit`;");
    }
}
