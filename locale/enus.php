<?php
return array(
    'repo_new'             => "Create new repository",
    'repo_type'         => 'Type',
    'repo_path'         => 'Path',
    'repo'              => 'Repository',
    'repo_overview'    => 'Overview',
    'repo_tree'         => 'Tree',
    'repo_history'      => 'History',
    'repo_commit'       => 'Commit',
    'repo_author'       => 'Author',
    'repo_date'         => 'Date',
    'repo_back'         => 'Back',
    'repo_info'         => 'Info',
    'repo_lastcommit'   => 'Last commit',
    'repo_name'         => 'Name',
    'repo_cloneurl'          => 'Clone-URL'
);

