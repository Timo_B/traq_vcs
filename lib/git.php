<?php
/*!
 * FishHook
 * Copyright (C) 2009-2012 Jack P.
 * https://github.com/nirix
 *
 * FishHook is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3 only.
 *
 * FishHook is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with FishHook. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * The FishHook plugin library
 *
 * @package FishHook
 * @author Jack P.
 * @copyright (C) 2009-2012 Jack P.
 * @version 4.0
 */

namespace Vcs\lib;

class Git
{
    private static $_version = '1.0';
    private static $_plugins = array();

    
    public static function info($path='')
    {
        $info = Git::_getRepoInfo($path);
        
        #$repo = Git::_getRepoLog($path);

        $repo = array(
            'info' => $info,
            
        );
        return $repo;
    } 


    public static function tree($path='', $dir='')
    {
        return Git::_getRepoTree($path, $dir);
    }

    public static function log($path='', $dir='')
    {
        return Git::_getRepoLog($path);
    }

    public static function commit($commit, $path)
    {
        return Git::_getCommitInfo($commit, $path);
    }

    public static function pull($path)
    {
        exec("cd ".$path." && git pull", $output, $retval);
    }

    private static function _getRepoTree($path, $dir = '')
    {
        $tree = array();
        if($dir){
            $dir = $dir . DIRECTORY_SEPARATOR;
        }
        exec("cd ".$path." && git ls-tree --name-only HEAD ".$dir, $output, $retval);
        $output = array_filter($output);
        foreach($output as $line){
            $item = array();
            $item['name'] = $line;
            if(is_dir($path . DIRECTORY_SEPARATOR . $line)){
                $item['type'] = 'd';
            } else {
                $item['type'] = 'f';
            }
            $tree[] = $item;
        }
        return $tree;
    }

    private static function _getRepoLog($path='')
    {
        $log = array();
        exec("cd ".$path." && git --no-pager log", $output, $retval);
        $output = array_filter($output);
        foreach ($output as $item) {
            $arr = explode(' ',$item);
            if($arr[0] == 'commit'){
                $commit_id = $arr[1];
            } else {
                $out[$commit_id][] = $item;
            }


        }
        return $out;

    }
    private static function _getCommitInfo($commit, $path)
    {
        $info = array();
        exec("cd ".$path." && git show --stat --oneline ".trim($commit)." --pretty=\"format:\"", $output, $retval);
        $output = array_filter($output);
        return $output;

    }

    

    private static function _getRepoInfo($path)
    {
        $info = array();
        $info['description'] = file_get_contents($path.DIRECTORY_SEPARATOR.'.git'.DIRECTORY_SEPARATOR.'description');
        exec("cd ".$path." && git log --max-count=1 HEAD", $output, $retval);
        $info['author'] = trim(str_replace('Author: ', '', $output[1]));
        $info['last_commit']['date'] =  trim(str_replace('Date: ', '', $output[2]));
        array_shift($output);
        array_shift($output);
        array_shift($output);
        $output = array_filter($output);
        $info['last_commit']['message'] = implode(PHP_EOL,$output);
        return $info;
    }  

    
}
