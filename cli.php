<?php
/*
 * Traq
 * Copyright (C) 2009-2012 Jack Polgar
 * 
 * This file is part of Traq.
 * 
 * Traq is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3 only.
 * 
 * Traq is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Traq. If not, see <http://www.gnu.org/licenses/>.
 */

define('START_TIME', microtime(true));
define('START_MEM',  memory_get_usage());


require '../../../bootstrap.php';

use avalon\core\Kernel as Avalon;

use Vcs\models\VcsModel;
use Vcs\models\VcslogModel;
use Vcs\models\VcscommitModel;

use Vcs\lib\Git;


foreach (VcsModel::fetch_all() as $repo) {
	// pull latest changes to local repo
	// user who runs the script/cronjob needs access to do this without using pw authentication
	// using ssh keys should work
	Git::pull($repo->path);

	// fetch log of repo
	$log = Git::log($repo->path);


	foreach($log as $key => $value){
		$entry = array();
		$entry['commit'] = trim($key);
		foreach ($value as $v) {
			if(preg_match('/^Author: /', $v)){
				$entry['author'] = str_replace('Author: ', '', $v);
			}
			elseif(preg_match('/^Date: /', $v)){
				$entry['date'] = str_replace('Date: ', '', $v);
			} else {
				$entry['message'][] = trim($v);
			}
		}

		// Check if commit already exists in Database and skip, import if not existing

		$exists = VcslogModel::find('commit', trim($entry['commit']));
		if(!$exists){
			$logentry = new VcslogModel;
			$logentry->set(array(
	            'author'         => trim($entry['author']),
	            'date'           => strtotime(trim($entry['date'])),
	            'commit'        => trim($entry['commit']),
	            'message' => implode(PHP_EOL, $entry['message']),
	            'project_id'    => $repo->project_id
	        ));
			$logentry->save();

			// same for details of commit
			$commit = implode(PHP_EOL, Git::commit(trim($entry['commit']), $repo->path));
			$commitentry = new VcscommitModel;
			$commitentry->set(array(
				'author'         => trim($entry['author']),
	            'date'           => strtotime(trim($entry['date'])),
	            'commit'        => trim($entry['commit']),
	            'info' => $commit,
	            'project_id'    => $repo->project_id
			));
			$commitentry->save();
		}
	}
}


