<?php
/*!
 * Traq
 * Copyright (C) 2009-2013 Traq.io
 *
 * This file is part of Traq.
 *
 * Traq is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3 only.
 *
 * Traq is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Traq. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Vcs\models;

/**
 * Custom tabs model.
 *
 * @author Jack P.
 * @since 3.0.7
 * @package CustomTabs
 * @subpackage Models
 */
class VcsModel extends \avalon\database\model
{
    protected static $_name = 'vcs';
    protected static $_properties = array(
        'id',
        'path',
        'name',
        'type',
        'project_id',
        'groups',
        'cloneurl',
        'display_order'
    );

    public function is_valid()
    {
        if (empty($this->_data['path'])) {
            $this->errors['path'] = l('errors.path_blank');
        }

       
        return !count($this->errors) > 0;
    }
}
