<?php
/*!
 * Traq
 * Copyright (C) 2009-2013 Traq.io
 *
 * This file is part of Traq.
 *
 * Traq is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3 only.
 *
 * Traq is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Traq. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Vcs\controllers;


use avalon\http\Request;
use avalon\output\View;

use Vcs\lib\Git;

use traq\helpers\Pagination;


use Vcs\models\VcsModel;
use Vcs\models\VcslogModel;
use Vcs\models\VcscommitModel;

/**
 * Custom tabs controller.
 *
 * @author Jack P.
 * @since 3.0.7
 * @package CustomTabs
 * @subpackage Controllers
 */
class Vcs extends \traq\controllers\AppController
{
    


    /**
     * Tab listing page.
     * Nothing to do here as the tabs are already sent to the view.
     */
    public function action_index($id) {
        
        $this->_checkAccess($id);
        

        $vcs = VcsModel::find($id);
        $repo = Git::info($vcs->path);

        View::set(compact('repo'));
        View::set(compact('vcs'));

        View::render('vcs/index');
    }

    public function action_commit($id, $commit)
    {
        $this->_checkAccess($id);

        $vcs = VcsModel::find($id);
        $commit = VcscommitModel::find('commit', trim($commit));

        View::set(compact('commit'));
        View::set(compact('vcs'));
        #var_export($commit);
    }

    public function action_history($id)
    {
        $this->_checkAccess($id);

        $vcs = VcsModel::find($id);
        
        $rows = $this->db->select()->from('vcs_log')->custom_sql('WHERE project_id='.$vcs->project_id);
        $rows = $rows->order_by('date', 'DESC');

        $pagination = new Pagination(
            (isset(Request::$request['page']) ? Request::$request['page'] : 1), // Page
            settings('tickets_per_page'), // Per page
            $rows->exec()->row_count() // Row count
        );

        if ($pagination->paginate) {
            $rows->limit($pagination->limit, settings('tickets_per_page'));
        }
        $log = array();
        foreach($rows->exec()->fetch_all() as $row) {
            $log[] = new VcslogModel($row, false);
        }

        View::set(compact('pagination'));
        View::set(compact('log'));
        View::set(compact('vcs'));

        View::render('vcs/history');
    
    }
    public function action_raw($id)
    {
        $this->_checkAccess($id);

        $vcs = VcsModel::find($id);
        if (Request::method() == 'post') {
            $file=$vcs->path.DIRECTORY_SEPARATOR.trim($_POST['file']); //file location
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="'.basename($file).'"');
            header('Content-Length: ' . filesize($file));
            readfile($file);
        }
    }
    public function action_tree($id)
    {
        $this->_checkAccess($id);

        $vcs = VcsModel::find($id);
        $tree = '';
        if(isset($_POST['dir'])){
            $tree = Git::tree($vcs->path, trim($_POST['dir']));
            $pdir = explode(DIRECTORY_SEPARATOR, trim($_POST['dir']));
            array_pop($pdir);
            if(!empty($_POST['dir'])){
                $parent = array(
                    'name' => '..',
                    'path' => implode(DIRECTORY_SEPARATOR, $pdir)
                );
            }
            
        } else {
            $tree = Git::tree($vcs->path);
        }
        

        View::set(compact('vcs'));
        View::set(compact('parent'));
        View::set(compact('tree'));

        View::render('vcs/tree');
    }

    private function _checkAccess($id)
    {
        $vcs = VcsModel::find($id);
        $current_user = $this->user;
        #var_export($id);die();
        if (($vcs->groups != 3) AND !in_array($current_user->group_id, explode(',', $vcs->groups))) { 
            Request::redirectTo('/login');
        }
    }


}
