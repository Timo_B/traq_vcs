<?php
/*!
 * Traq
 * Copyright (C) 2009-2013 Traq.io
 *
 * This file is part of Traq.
 *
 * Traq is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3 only.
 *
 * Traq is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Traq. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Vcs\controllers\admin;

use avalon\http\Request;
use avalon\output\View;

use Vcs\models\VcsModel;

/**
 * Custom tabs controller.
 *
 * @author Jack P.
 * @since 3.0.7
 * @package CustomTabs
 * @subpackage Controllers
 */
class Vcs extends \traq\controllers\admin\AppController
{
    /**
     * Tab listing page.
     * Nothing to do here as the tabs are already sent to the view.
     */
    public function action_index() {}


    /**
     * New tab.
     */
    public function action_new()
    {
        $tab = new VcsModel;

        // Check if the form has been submitted.
        if (Request::method() == 'post') {
            $tab->set(array(
                'path'         => Request::post('path'),
                'type'           => Request::post('type'),
                'name'          => Request::post('name'),
                'cloneurl'      => Request::post('cloneurl'),
                'groups'        => implode(',', Request::post('groups', \traq\models\Group::all_group_ids())),
                'display_order' => Request::post('display_order', 0),
                'project_id'    => Request::post('project_id', 0)
            ));

            // Save and reidrect
            if ($tab->save()) {
                Request::redirectTo('/admin/vcs');
            }
        }

        View::set(compact('tab'));
    }

    /**
     * Edit tab.
     *
     * @param integer $id Tab ID
     */
    public function action_edit($id)
    {
        $tab = VcsModel::find($id);

        // Check if the form has been submitted.
        if (Request::method() == 'post') {
            $tab->set(array(
                'path'         => Request::post('label', $tab->path),
                'type'           => Request::post('url', $tab->type),
                'name'          => Request::post('name'),
                'cloneurl'      => Request::post('cloneurl'),
                'groups'        => implode(',', Request::post('groups', explode(',', $tab->groups))),
                'display_order' => Request::post('display_order', $tab->display_order),
                'project_id'    => Request::post('project_id', $tab->project_id)
            ));

            // Save and redirect
            if ($tab->save()) {
                Request::redirectTo('/admin/vcs');
            }
        }

        View::set(compact('tab'));
    }

    /**
     * Delete tab.
     *
     * @param integer $id Tab ID
     */
    public function action_delete($id)
    {
        VcsModel::find($id)->delete();
        Request::redirectTo('/admin/vcs');
    }
}
